/* Send Multicast Datagram code example. */
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>


#include "v4l2_func.h"

//Multicast socket for sending images
struct in_addr localInterface;
struct sockaddr_in groupSock;
int sd;


//create socket for sending multicast img, reg to grp
void create_socket_and_register(void)
{
	/* Create a datagram socket on which to send. */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
	{
	  perror("Opening datagram socket error");
	  exit(1);
	}
	else
	  printf("Opening the datagram socket...OK.\n");
	 
	/* Initialize the group sockaddr structure with a */
	/* group address of 225.1.1.1 and port 5555. */
	memset((char *) &groupSock, 0, sizeof(groupSock));
	groupSock.sin_family = AF_INET;
	groupSock.sin_addr.s_addr = inet_addr("226.1.1.1");
	groupSock.sin_port = htons(4321);
	 
	/* Set local interface for outbound multicast datagrams. */
	/* The IP address specified must be associated with a local, */
	/* multicast capable interface. */
	//localInterface.s_addr = inet_addr("203.106.93.94");
	localInterface.s_addr = INADDR_ANY;
	if(setsockopt(sd, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
	{
	  perror("Setting local interface error");
	  exit(1);
	}
	else
	  printf("Setting the local interface...OK\n");

}


int main (int argc, char *argv[ ])
{
	sprintf(dev_name , "/dev/video1"); //dev_name = "/dev/video2";
	io = IO_METHOD_USERPTR; //IO_METHOD_MMAP;
	fd = -1;
	frame_count = 30;
	frame_number = 0;

	//V4L2 SETUP
	open_device();
	init_device();
	start_capturing();

	//sock to send images on multicast
	create_socket_and_register();

	while(1)
	{
		//Send an image to the multicast group specified by the groupSock sockaddr structure:
		
		//dque a buffer and get the ptr to buffer
		struct v4l2_buffer buf;
		grab_frame(&buf);
		
		//check filesize is less than UDP limits
		if(buf.bytesused > 64000)
		{
			printf("ERROR: filesize greater than limit of 64Kb\n");
			enque_buffer(&buf);
			continue;
		}

		//Copy data from buffer to socket
		int retcount=0;
		char* pImgBuf = (char*)buf.m.userptr;
		int32_t filesize = buf.bytesused;
		do
		{
			retcount = sendto(sd, (void *)pImgBuf, buf.bytesused, 0, (struct sockaddr*)&groupSock, sizeof(groupSock));
			if (retcount < 0)
			{
			  perror("write: image");
			  return -1;
			}
			else
			{
			  pImgBuf += retcount;
			  filesize -= retcount;
			}
		}
		while (filesize > 0);
		frame_number++;
		printf("Sending datagram img...OK - %d\n", frame_number);
		
		//put the v4l2 buffer back into que
		enque_buffer(&buf);

	}
	
	stop_capturing();
	uninit_device();
	close_device();
	return 0;
}


