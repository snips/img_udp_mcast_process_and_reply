#include <iostream>       // std::cin, std::cout
#include <queue>          // std::queue
#include <stdlib.h>     /* atoi */  
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/aruco.hpp>

using namespace std;
using namespace cv;

const int BUF_MAX_SIZE = 64000;

struct imgBuf
{
	char buf[BUF_MAX_SIZE];
	int bufSize;
};

imgBuf inBuf;

std::queue<imgBuf> que_imgBuf;

//For listening multicast socket
struct sockaddr_in localSock;
struct ip_mreq group;
int sd;
int datalen;
int frame_number = 0;

//Multicast socket for sending abck results
struct in_addr localInterface;
struct sockaddr_in groupSock;
int sd2;

//This is for listening to multicast socket
void create_socket_and_bind(void)
{
	/* Create a datagram socket on which to receive. */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
	{
		perror("Opening datagram socket error");
		exit(1);
	}
	else
		printf("Opening datagram socket....OK.\n");

	/* Enable SO_REUSEADDR to allow multiple instances of this */
	/* application to receive copies of the multicast datagrams. */
	{
		int reuse = 1;
		if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
		{
			perror("Setting SO_REUSEADDR error");
			close(sd);
			exit(1);
		}
		else
			printf("Setting SO_REUSEADDR...OK.\n");
	}

	/* Bind to the proper port number with the IP address */
	/* specified as INADDR_ANY. */
	memset((char *) &localSock, 0, sizeof(localSock));
	localSock.sin_family = AF_INET;
	localSock.sin_port = htons(4321);
	localSock.sin_addr.s_addr = INADDR_ANY;
	if(bind(sd, (struct sockaddr*)&localSock, sizeof(localSock)))
	{
		perror("Binding datagram socket error");
		close(sd);
		exit(1);
	}
	else
		printf("Binding datagram socket...OK.\n");

}

void join_multicast_grp(void)
{
	/* Join the multicast group 226.1.1.1 on the local 203.106.93.94 */
	/* interface. Note that this IP_ADD_MEMBERSHIP option must be */
	/* called for each local interface over which the multicast */
	/* datagrams are to be received. */
	group.imr_multiaddr.s_addr = inet_addr("226.1.1.1");
	//group.imr_interface.s_addr = inet_addr("203.106.93.94");
	//SHREK:
	group.imr_interface.s_addr = INADDR_ANY;
	if(setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
	{
		perror("Adding multicast group error");
		close(sd);
		exit(1);
	}
	else
		printf("Adding multicast group...OK.\n");
}

//This is to send messages on multicast
void create_socket_and_register(void)
{
	/* Create a datagram socket on which to send. */
	sd2 = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd2 < 0)
	{
	  perror("Opening datagram socket error");
	  exit(1);
	}
	else
	  printf("Opening the datagram socket...OK.\n");
	 
	/* Initialize the group sockaddr structure with a */
	/* group address of 225.1.1.1 and port 5555. */
	memset((char *) &groupSock, 0, sizeof(groupSock));
	groupSock.sin_family = AF_INET;
	groupSock.sin_addr.s_addr = inet_addr("226.1.1.2");
	groupSock.sin_port = htons(7321);
	 
	/* Set local interface for outbound multicast datagrams. */
	/* The IP address specified must be associated with a local, */
	/* multicast capable interface. */
	//localInterface.s_addr = inet_addr("203.106.93.94");
	localInterface.s_addr = INADDR_ANY;
	if(setsockopt(sd2, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
	{
	  perror("Setting local interface error");
	  exit(1);
	}
	else
	  printf("Setting the local interface...OK\n");

}

int main ()
{
	//listen for images being multicast
	create_socket_and_bind();
	join_multicast_grp();

	//Send data back to sender on diff group
    create_socket_and_register();
	
	while(1)
	{
		/* Read from the socket. */
		int readret = read(sd, inBuf.buf, BUF_MAX_SIZE);
		if( readret < 0)
		{
			perror("Reading datagram message error");
			close(sd);
			exit(1);
		}
		
		//printf("Reading datagram message...OK.\n");
		//printf("The message from multicast server is: \"%s\"\n", databuf);

  		//update the received frame number
  		frame_number++;

  		/*
		//Add the image to a que
		inBuf.bufSize = readret;
		que_imgBuf.push (inBuf);
		if(que_imgBuf.size() > 2)
  			que_imgBuf.pop();
     	
  		//take out the image from que
        imgBuf outBuf;
        outBuf = que_imgBuf.front();
        que_imgBuf.pop(); 
        */


  		//process image
        cv::Mat image = cv::Mat(cv::Size(640,480), CV_8UC1, inBuf.buf);
        cv::Mat converted = cv::imdecode(image, CV_LOAD_IMAGE_COLOR);
        cv::imshow("img", converted);
        cv::waitKey(40);
     

        //Send the results
        char processed_image[100];
        sprintf(processed_image, "process2: The number of frames processed = %d", frame_number);
		int retcount=0;
		void* pImgBuf = processed_image;
		int32_t filesize = strlen(processed_image);
		do
		{
			retcount = sendto(sd2, (void *)pImgBuf, filesize, 0, (struct sockaddr*)&groupSock, sizeof(groupSock));
			if (retcount < 0)
			{
			  perror("write: image result");
			  return -1;
			}
			else
			{
			  pImgBuf += retcount;
			  filesize -= retcount;
			}
		}
		while (filesize > 0);
		printf("Image recvd. Sending reply...OK - %d\n",frame_number);      
		
	}

	return 0;
}