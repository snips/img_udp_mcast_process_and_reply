/* Send Multicast Datagram code example. */
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>

//For listening multicast socket
struct sockaddr_in localSock;
struct ip_mreq group;
int sd;
int datalen;
const int BUF_MAX_SIZE = 64000;

//This is for listening to multicast socket
void create_socket_and_bind(void)
{
	/* Create a datagram socket on which to receive. */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
	{
		perror("Opening datagram socket error");
		exit(1);
	}
	else
		printf("Opening datagram socket....OK.\n");

	/* Enable SO_REUSEADDR to allow multiple instances of this */
	/* application to receive copies of the multicast datagrams. */
	{
		int reuse = 1;
		if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
		{
			perror("Setting SO_REUSEADDR error");
			close(sd);
			exit(1);
		}
		else
			printf("Setting SO_REUSEADDR...OK.\n");
	}

	/* Bind to the proper port number with the IP address */
	/* specified as INADDR_ANY. */
	memset((char *) &localSock, 0, sizeof(localSock));
	localSock.sin_family = AF_INET;
	localSock.sin_port = htons(7321);
	//localSock.sin_addr.s_addr = INADDR_ANY; //this line gives errors. Changed to the one below.
	inet_aton("226.1.1.2", &localSock.sin_addr);

	if(bind(sd, (struct sockaddr*)&localSock, sizeof(localSock)))
	{
		perror("Binding datagram socket error");
		close(sd);
		exit(1);
	}
	else
		printf("Binding datagram socket...OK.\n");

}
void join_multicast_grp(void)
{
	/* Join the multicast group 226.1.1.1 on the local 203.106.93.94 */
	/* interface. Note that this IP_ADD_MEMBERSHIP option must be */
	/* called for each local interface over which the multicast */
	/* datagrams are to be received. */
	group.imr_multiaddr.s_addr = inet_addr("226.1.1.2");
	//group.imr_interface.s_addr = inet_addr("203.106.93.94");
	//SHREK:
	group.imr_interface.s_addr = INADDR_ANY;
	if(setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
	{
		perror("Adding multicast group error");
		close(sd);
		exit(1);
	}
	else
		printf("Adding multicast group...OK.\n");
}

int main (int argc, char *argv[ ])
{
	//sock to listen for result of processed images being multicast
	create_socket_and_bind();
	join_multicast_grp();

	//Listen for replies
	char inBuf[BUF_MAX_SIZE];
	char process1_header[100]="process1: ";
	char process2_header[100]="process2: ";

	while(1)
	{
		int readret = read(sd, inBuf, BUF_MAX_SIZE);
		if( readret < 0)
		{
			perror("Reading datagram message error");
			close(sd);
			exit(1);
		}
		printf("received reply: %s\n", inBuf);
		//if(0 == memcmp(inBuf, process1_header, strlen(process1_header)) )
		//	printf("Process 1: %s", readret+strlen(process1_header));
	}

	return 0;
}